#!/usr/bin/env python3
#! coding: utf-8

import csv
import logging
import os
import re
from datetime import datetime
from urllib import request
from xml.etree import ElementTree as ET

import aiohttp
import asyncio
import async_timeout
from logging.handlers import RotatingFileHandler

from .helper import CaseInsensitiveDict
from .settings import DEBUG, LOG, FILE_DEBUG, PREFIX, CSV_DELIMITER, TAXES


formatter = logging.Formatter(LOG['FORMAT'], datefmt=LOG['DATE'])
handler = RotatingFileHandler(
    FILE_DEBUG, maxBytes=2*1024*1024, backupCount=3, encoding='UTF-8')
handler.setFormatter(formatter)

if DEBUG:
    logging.basicConfig(
        level=logging.DEBUG, format=LOG['FORMAT'], datefmt=LOG['DATE'])
    handler.setLevel(logging.DEBUG)
else:
    logging.basicConfig(
        level=logging.INFO, format=LOG['FORMAT'], datefmt=LOG['DATE'])
    handler.setLevel(logging.INFO)

log = logging.getLogger(LOG['NAME'])
log.addHandler(handler)


def validate_rfc(value):
    if len(value) < 12:
        msg = 'Longitud inválida del RFC'
        return msg
    l = 4
    if len(value)==12:
        l = 3
    s = value[0:l]
    r = re.match('[A-ZÑ&]{%s}' % l, s)
    msg = 'Caracteres inválidos al {} del RFC'
    if not r:
        return msg.format('inicio')
    s = value[-3:]
    r = re.match('[A-Z0-9]{3}', s)
    if not r:
        return msg.format('final')
    s = value[l:l+6]
    r = re.match('[0-9]{6}', s)
    msg = 'Fecha inválida en el RFC'
    if not r:
        return msg
    try:
        datetime.strptime(s, '%y%m%d')
        return ''
    except:
        return msg


def validate_folder(path, read=True, write=True):
    msg = ''
    if not os.path.exists(path):
        msg = 'No existe el directorio: {}'.format(path)
    if read and not os.access(path, os.R_OK):
        msg = 'No tienes derecho de lectura en el directorio: {}'.format(path)
    if write and not os.access(path, os.W_OK):
        msg = 'No tienes derecho de escritura en el directorio: {}'.format(path)
    return msg


def validate_date(year=0, month=0, day=0, date_str=''):
    try:
        if date_str:
            parts = date_str.split('/')
            if len(parts) != 3:
                parts = date_str.split('-')
            if len(parts) != 3:
                return 'Fecha inválida'
            day = int(parts[0])
            month = int(parts[1])
            if len(parts[2]) == 2:
                year = int('20' + parts[2])
            else:
                year = int(parts[2])
        d = datetime(year, month, day)
        return d
    except ValueError:
        msg = 'Fecha de búsqueda inválida'
        return msg


def get_path_info(path):
    path, filename = os.path.split(path)
    name, extension = os.path.splitext(filename)
    return path, filename, name, extension


def exists(path, check_size=True):
    ok = os.path.exists(path)
    if check_size:
        return ok and bool(os.path.getsize(path))
    else:
        return ok


def get_home():
    return os.path.expanduser('~')


def join(*paths):
    return os.path.join(*paths)


def get_files(path, ext='xml'):
    docs = []
    for folder, _, files in os.walk(path):
        pattern = re.compile('\.{}'.format(ext), re.IGNORECASE)
        docs += [os.path.join(folder,f) for f in files if pattern.search(f)]
    return tuple(docs)


def get_headers_nomina(file_xml):
    try:
        root = ET.parse(file_xml).getroot()
    except Exception as e:
        log.error(path)
        log.error(e)
        return (), ()

    headers_p = []
    headers_d = []
    if 'version' in root.attrib:
        version = root.attrib['version']
    else:
        version = root.attrib['Version']
    pre = PREFIX[version]

    complemento = root.find('{}Complemento'.format(pre))
    if complemento is None:
        return (), ()

    nomina = complemento[0]
    pre = '{http://www.sat.gob.mx/nomina}'
    if 'Version' in nomina.attrib:
        versiones = {'1.2': '{http://www.sat.gob.mx/nomina12}'}
        pre = versiones.get(nomina.attrib['Version'], pre)

    node = nomina.find('{}Percepciones'.format(pre))
    if node is not None:
        for n in node.getchildren():
            if not 'Clave' in n.attrib:
                continue
            name = 'percepcion_{}_{}'.format(
                n.attrib['Clave'], n.attrib['Concepto'].lower())
            headers_p.append((n.attrib['Clave'], name + '_gravado'))
            headers_p.append((n.attrib['Clave'], name + '_exento'))

    node = nomina.find('{}Deducciones'.format(pre))
    if node is not None:
        for n in node.getchildren():
            name = 'deduccion_{}_{}'.format(
                n.attrib['Clave'], n.attrib['Concepto'].lower())
            headers_d.append((n.attrib['Clave'], name + '_gravado'))
            headers_d.append((n.attrib['Clave'], name + '_exento'))
    return tuple(headers_p), tuple(headers_d)


def _get_info_33(version, root, opt, i, t):
    pre = PREFIX[version]
    data = root.attrib.copy()
    del data['Sello']
    del data['Certificado']
    try:
        del data['{http://www.w3.org/2001/XMLSchema-instance}schemaLocation']
    except:
        pass

    complemento = root.find('{}Complemento'.format(pre))
    if complemento is None:
        return

    # ~ nomina = complemento[0]
    prenom = '{http://www.sat.gob.mx/nomina}'
    nomina = root.find('{}Complemento/{}Nomina'.format(pre, prenom))
    if nomina is None:
        prenom = '{http://www.sat.gob.mx/nomina12}'
        nomina = root.find('{}Complemento/{}Nomina'.format(pre, prenom))

    # Timbre
    node = root.find(
        '{}Complemento/{}TimbreFiscalDigital'.format(pre, PREFIX['TIMBRE']))
    if not node is None:
        data['UUID'] = node.attrib['UUID'].upper()
        data['FechaTimbrado'] = node.attrib['FechaTimbrado'].replace('T', ' ')
        data['fecha'] = data['Fecha'].replace('T', ' ')

    if nomina is not None:
        data.update(nomina.attrib.copy())
        node = nomina.find('{}Percepciones'.format(prenom))
        if node is not None:
            for k, v in node.attrib.items():
                name = 'percepciones{}'.format(k.lower())
                data[name] = v
            for n in node.getchildren():
                if not 'Clave' in n.attrib:
                    continue
                name = 'percepcion{}{}'.format(
                    n.attrib['Clave'], n.attrib['Concepto'].lower())
                data[name + 'gravado'] = n.attrib['ImporteGravado']
                data[name + 'exento'] = n.attrib['ImporteExento']

        node = nomina.find('{}Deducciones'.format(prenom))
        if node is not None:
            for k, v in node.attrib.items():
                name = 'deducciones{}'.format(k.lower())
                data[name] = v
            for n in node.getchildren():
                name = 'deduccion{}{}'.format(
                    n.attrib['Clave'], n.attrib['Concepto'].lower())
                if 'ImporteGravado' in n.attrib:
                    data[name + 'gravado'] = n.attrib['ImporteGravado']
                if 'ImporteExento' in n.attrib:
                    data[name + 'exento'] = n.attrib['ImporteExento']
                if 'Importe' in n.attrib:
                    data[name] = n.attrib['Importe']

    if opt.fecha_inicial:
        if opt.fecha_final is None:
            opt.fecha_final = datetime.now().replace(
                hour=23, minute=59, second=59, microsecond=0)
        cfdi_date = datetime.strptime(data['fecha'], '%Y-%m-%dT%H:%M:%S')
        if cfdi_date < opt.fecha_inicial or cfdi_date > opt.fecha_final:
            return False

    # Emisor and Receptor
    node = root.find('{}Emisor'.format(pre))
    if node is not None:
        rfc = node.attrib['Rfc']
        if opt.rfc_emisor and not opt.rfc_emisor == rfc:
            return False
        data['emisorrfc'] = rfc
        data['emisor'] = node.attrib.get('Nombre', '')

    if nomina is not None:
        node = nomina.find('{}Emisor'.format(prenom))
        if not node is None:
            name = 'RegistroPatronal'
            if name in node.attrib:
                data[name.lower()] = node.attrib[name]

    node = root.find('{}Receptor'.format(pre))
    if node is not None:
        rfc = node.attrib['Rfc']
        if opt.rfc_receptor and not opt.rfc_receptor == rfc:
            return False
        data['receptorrfc'] = rfc
        data['receptor'] = node.attrib.get('Nombre', '')

    if nomina is not None:
        node = nomina.find('{}Receptor'.format(prenom))
        if not node is None:
            data.update(node.attrib)

    # Impuestos
    node = root.find('{}Impuestos'.format(pre))
    if node is not None:
        data.update(node.attrib)
        imp = node.find('{}Traslados'.format(pre))
        if imp is not None:
            for n in list(imp):
                type_tax = TAXES[n.attrib['Impuesto']]
                tasa = int(float(n.attrib['TasaOCuota']) * 100)
                key = 'traslado{}{}'.format(type_tax, tasa)
                if key in data:
                    data[key] += float(n.attrib['Importe'])
                else:
                    data[key] = float(n.attrib['Importe'])

        imp = node.find('{}Retenciones'.format(pre))
        if imp is not None:
            for n in list(imp):
                key = 'retencion{}'.format(n.attrib['Impuesto'])
                data[key] = float(n.attrib['Importe'])

    # Conceptos
    fields_details = (
        'noidentificacion',
        'descripcion',
        'unidad',
        'cantidad',
        'valorunitario',
        'importe'
    )
    details = []
    node = root.find('{}Conceptos'.format(pre))
    if not 'conceptos' in opt.campos and \
        any(d in opt.campos for d in fields_details):
        for n in node.getchildren():
            details.append(n.attrib.copy())
    elif 'conceptos' in opt.campos:
        for n in node.getchildren():
            line = 'Clave: {}, {} [${:,.2f}]'.format(
                n.attrib.get('NoIdentificacion', ''),
                n.attrib['Descripcion'],
                float(n.attrib['ValorUnitario']))
            details.append(line)
        data['conceptos'] = ' | '.join(details)

    if not 'conceptos' in opt.campos and details:
        for d in details:
            info = []
            data2 = data.copy()
            data2.update(d)
            data2 = CaseInsensitiveDict(data2)
            for f in opt.campos:
                v = data2.get(f, '')
                info.append(v)
            return info
    else:
        if 'estatussat' in opt.campos:
            sat = {
                'emisor_rfc': data['emisorrfc'],
                'receptor_rfc': data['receptorrfc'],
                'total': data['Total'],
                'uuid': data['UUID']
            }
            #~ data['estatussat'] = get_status_sat(sat, i, t)
            data['estatussat'] = sat

        info = []
        data = CaseInsensitiveDict(data)
        for f in opt.campos:
            v = data.get(f, '')
            info.append(v)
        return info


def get_info_xml(file_xml, opt, i, t):
    try:
        root = ET.parse(file_xml).getroot()
    except Exception as e:
        log.error(file_xml)
        log.error(e)
        return False
    # ~ print (i + 1, file_xml)
    key = 'Version'
    if 'version' in root.attrib:
        key = 'version'
    version = root.attrib[key]

    if version == '3.3':
        yield _get_info_33(version, root, opt, i, t)
        return

    pre = PREFIX[version]
    data = root.attrib.copy()
    del data['sello']
    del data['certificado']
    del data['{http://www.w3.org/2001/XMLSchema-instance}schemaLocation']

    prenom = '{http://www.sat.gob.mx/nomina}'
    nomina = root.find('{}Complemento/{}Nomina'.format(pre, prenom))
    if nomina is None:
        prenom = '{http://www.sat.gob.mx/nomina12}'
        nomina = root.find('{}Complemento/{}Nomina'.format(pre, prenom))

    if opt.nomina and nomina is None:
        return False


    # ~ node = root.find(
        # ~ '{}Complemento/{}TimbreFiscalDigital'.format(pre, PREFIX['TIMBRE']))
    # ~ if node is not None:
        # ~ print(data['fecha'], node.attrib['UUID'].upper())


    if nomina is not None:
        data.update(nomina.attrib.copy())
        node = nomina.find('{}Percepciones'.format(prenom))
        if node is not None:
            for k, v in node.attrib.items():
                name = 'percepciones{}'.format(k.lower())
                data[name] = v
            for n in node.getchildren():
                if not 'percepcion' in n.tag.lower():
                    continue
                name = 'percepcion{}{}'.format(
                    n.attrib['Clave'], n.attrib['Concepto'].lower())
                data[name + 'gravado'] = n.attrib['ImporteGravado']
                data[name + 'exento'] = n.attrib['ImporteExento']

        node = nomina.find('{}Deducciones'.format(prenom))
        if node is not None:
            for k, v in node.attrib.items():
                name = 'deducciones{}'.format(k.lower())
                data[name] = v
            for n in node.getchildren():
                name = 'deduccion{}{}'.format(
                    n.attrib['Clave'], n.attrib['Concepto'].lower())
                if 'ImporteGravado' in n.attrib:
                    data[name + 'gravado'] = n.attrib['ImporteGravado']
                if 'ImporteExento' in n.attrib:
                    data[name + 'exento'] = n.attrib['ImporteExento']
                if 'Importe' in n.attrib:
                    data[name] = n.attrib['Importe']

    if opt.fecha_inicial:
        if opt.fecha_final is None:
            opt.fecha_final = datetime.now().replace(
                hour=23, minute=59, second=59, microsecond=0)
        cfdi_date = datetime.strptime(data['fecha'], '%Y-%m-%dT%H:%M:%S')
        if cfdi_date < opt.fecha_inicial or cfdi_date > opt.fecha_final:
            return False

    # Emisor and Receptor
    node = root.find('{}Emisor'.format(pre))
    if node is not None:
        rfc = node.attrib['rfc']
        if opt.rfc_emisor and not opt.rfc_emisor == rfc:
            return False
        data['emisorrfc'] = rfc
        data['emisor'] = node.attrib.get('nombre', '')
    node = node.find('{}DomicilioFiscal'.format(pre))
    if node is not None:
        for k, v in node.attrib.items():
            kn = 'emisor{}'.format(k)
            data[kn] = v

    if nomina is not None:
        node = nomina.find('{}Emisor'.format(prenom))
        if not node is None:
            name = 'RegistroPatronal'
            if name in node.attrib:
                data[name.lower()] = node.attrib[name]

    node = root.find('{}Receptor'.format(pre))
    if node is not None:
        rfc = node.attrib['rfc']
        if opt.rfc_receptor and not opt.rfc_receptor == rfc:
            return False
        data['receptorrfc'] = rfc
        data['receptor'] = node.attrib.get('nombre', '')
    node = node.find('{}Domicilio'.format(pre))
    if node is not None:
        for k, v in node.attrib.items():
            kn = 'receptor{}'.format(k)
            data[kn] = v

    if nomina is not None:
        node = nomina.find('{}Receptor'.format(prenom))
        if not node is None:
            data.update(node.attrib)

    # Impuestos
    node = root.find('{}Impuestos'.format(pre))
    if node is not None:
        data.update(node.attrib)
        imp = node.find('{}Traslados'.format(pre))
        if imp is not None:
            for n in list(imp):
                key = 'traslado{}{}'.format(
                    n.attrib['impuesto'], int(float(n.attrib['tasa'])))
                if key in data:
                    data[key] += float(n.attrib['importe'])
                else:
                    data[key] = float(n.attrib['importe'])
        imp = node.find('{}Retenciones'.format(pre))
        if imp is not None:
            for n in list(imp):
                key = 'retencion{}'.format(n.attrib['impuesto'])
                data[key] = float(n.attrib['importe'])

    # Timbre
    node = root.find(
        '{}Complemento/{}TimbreFiscalDigital'.format(pre, PREFIX['TIMBRE']))
    if node is not None:
        data['UUID'] = node.attrib['UUID'].upper()
        data['FechaTimbrado'] = node.attrib['FechaTimbrado'].replace('T', ' ')
        data['fecha'] = data['fecha'].replace('T', ' ')

    # Conceptos
    fields_details = (
        'noidentificacion',
        'descripcion',
        'unidad',
        'cantidad',
        'valorunitario',
        'importe'
    )
    details = []
    node = root.find('{}Conceptos'.format(pre))
    if not 'conceptos' in opt.campos and \
        any(d in opt.campos for d in fields_details):
        for n in node.getchildren():
            details.append(n.attrib.copy())
    elif 'conceptos' in opt.campos:
        for n in node.getchildren():
            line = 'Clave: {}, {} [${:,.2f}]'.format(
                n.attrib.get('noIdentificacion', ''),
                n.attrib['descripcion'],
                float(n.attrib['valorUnitario']))

            cv = ''
            nc = n.find('{}ComplementoConcepto'.format(pre))
            if not nc is None:
                nv = nc.find('{}VentaVehiculos'.format('{http://www.sat.gob.mx/ventavehiculos}'))
                if not nv is None:
                    cv = nv.attrib['ClaveVehicular']
            if cv:
                line += ' Clave Vehicular: {}'.format(cv)

            details.append(line)
        data['conceptos'] = ' | '.join(details)

    if not 'conceptos' in opt.campos and details:
        for d in details:
            info = []
            data2 = data.copy()
            data2.update(d)
            data2 = CaseInsensitiveDict(data2)
            for f in opt.campos:
                v = data2.get(f, '')
                info.append(v)
            yield info
    else:
        if 'estatussat' in opt.campos:
            sat = {
                'emisor_rfc': data['emisorrfc'],
                'receptor_rfc': data['receptorrfc'],
                'total': data['total'],
                'uuid': data['UUID']
            }
            #~ data['estatussat'] = get_status_sat(sat, i, t)
            data['estatussat'] = sat

        info = []
        data = CaseInsensitiveDict(data)
        for f in opt.campos:
            v = data.get(f, '')
            info.append(v)
        yield info


def get_status_sat(data, i=0, t=0):
    if i and t:
        log.info('{} de {}: Obteniendo estatus del SAT: {}'.format(i, t, data['uuid']))
    else:
        log.info('Obteniendo estatus del SAT: {}'.format(data['uuid']))
    webservice = 'https://consultaqr.facturaelectronica.sat.gob.mx/consultacfdiservice.svc'
    soap = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Header/>
        <soap:Body>
        <Consulta xmlns="http://tempuri.org/">
            <expresionImpresa>
                ?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}
            </expresionImpresa>
        </Consulta>
        </soap:Body>
    </soap:Envelope>"""
    data = soap.format(**data).encode('utf-8')
    headers = {
        'SOAPAction': '"http://tempuri.org/IConsultaCFDIService/Consulta"',
        'Content-length': len(data),
        'Content-type': 'text/xml; charset="UTF-8"'
    }
    req = request.Request(url=webservice, data=data, method='POST')
    for k, v in headers.items():
        req.add_header(k, v)
    try:
        with request.urlopen(req, timeout=5) as f:
            response = f.read().decode('utf-8')
        result = re.search("(?s)(?<=Estado>).+?(?=</a:)", response).group()
        log.info('\tEstatus del SAT: {}'.format(result))
        return result
    except Exception as e:
        log.error(str(e))
        return 'Error SAT'


def save_csv(rows, path):
    log.info('Guardando archivo CSV: {}'.format(path))
    with open(path, 'w', newline='\n', encoding='utf-8') as csvfile:
        csvwriter = csv.writer(
            csvfile, delimiter=CSV_DELIMITER, quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerows(rows)
    return


def _clean(value):
    value = value.strip().replace('-', '').replace('_', '').lower()
    if value == 'imss':
        value = 'NumSeguridadSocial'
    return value


def get_fields_nomina(files):
    log.info('Obteniendo encabezados de nomina...')
    fields = []
    p = ()
    d = ()
    for f in files:
        p1, d1 = get_headers_nomina(f)
        p += p1
        d += d1
    p = tuple(set(p))
    d = tuple(set(d))
    p = sorted(p, key=lambda x: x[0])
    d = sorted(d, key=lambda x: x[0])
    _, p = zip(*p)
    _, d = zip(*d)
    fields = p + d + (
        'percepciones_total_gravado',
        'percepciones_total_exento',
        'deducciones_total_gravado',
        'deducciones_total_exento',
        'total',
    )
    return fields


def cfdi_reporte(files, opt):
    log.info('Iniciando reporte...')
    fields = opt.campos.split('|')

    if opt.nomina:
        fields += get_fields_nomina(files)
    lines = [tuple(fields)]
    opt.campos = tuple([_clean(f) for f in fields])
    log.info('Obteniendo información de los XML...')
    t = len(files)
    for i, f in enumerate(files):
        lines += get_info_xml(f, opt, i, t)

    log.info('Obteniendo estatus del SAT...')
    if 'estatussat' in opt.campos:
        lines = _get_status_sat(lines)

    log.info('Guardando archivo...')
    save_csv(lines, opt.archivo)
    log.info('Reporte terminado...')
    return


async def _post(session, data):
    webservice = 'https://consultaqr.facturaelectronica.sat.gob.mx' \
        '/consultacfdiservice.svc'
    webservice = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc'
    async with session.post(webservice, data=data, timeout=None) as response:
        try:
            assert response.status == 200
            html = await response.text()
            return re.search("(?s)(?<=Estado>).+?(?=</a:)", html).group()
        except AssertionError:
            return 'Error {}'.format(response.status)


async def _status_sat(rows):
    soap = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Header/>
        <soap:Body>
        <Consulta xmlns="http://tempuri.org/">
            <expresionImpresa>
                ?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}
            </expresionImpresa>
        </Consulta>
        </soap:Body>
    </soap:Envelope>"""
    headers = {
        'SOAPAction': '"http://tempuri.org/IConsultaCFDIService/Consulta"',
        'Content-type': 'text/xml; charset="UTF-8"'
    }
    tasks = []

    args = {
        'headers': headers,
        'connector': aiohttp.TCPConnector(verify_ssl=False)
    }
    async with aiohttp.ClientSession(**args) as session:
        for row in rows:
            data = soap.format(**row).encode('utf-8')
            task = asyncio.ensure_future(_post(session, data))
            tasks.append(task)

        return await asyncio.gather(*tasks)


def _get_status_sat(rows):
    col = rows[0].index('estatus_sat')
    data = [row[col] for row in rows[1:]]

    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(_status_sat(data))
    result = loop.run_until_complete(future)

    for i, row in enumerate(rows[1:]):
        row[col] = result[i]

    return rows
