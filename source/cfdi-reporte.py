#!/usr/bin/env python3
#! coding: utf-8

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.


import argparse
import logging
from logging.handlers import RotatingFileHandler

from cfdireporte import util
from cfdireporte.settings import DEBUG, LOG


log = logging.getLogger(LOG['NAME'])


def date(value):
    if value:
        msg = 'Fecha en formato incorrecto, usa: D/M/AAAA o D-M-AAAA'
        result = util.validate_date(date_str=value)
        if isinstance(result, str):
            raise argparse.ArgumentTypeError(msg)
        return result
    return value


def rfc(value):
    if value:
        msg = util.validate_rfc(value.upper())
        if msg:
            raise argparse.ArgumentTypeError(msg)
    return value.upper()


def folder(value):
    if value:
        msg = util.validate_folder(value, write=False)
        if msg:
            raise argparse.ArgumentTypeError(msg)
    return value


def folder_write(value):
    if value:
        path, _, _, _ = util.get_path_info(value)
        if not path:
            return value
        msg = util.validate_folder(path, read=False)
        if msg:
            raise argparse.ArgumentTypeError(msg)
    return value


def campos(value):
    if not value.strip():
        msg = 'Se requiere al menos un campo'
        raise argparse.ArgumentTypeError(msg)

    fields = value.split('|')
    if not fields:
        msg = 'Se requiere al menos un campo'
        raise argparse.ArgumentTypeError(msg)

    return value


def process_command_line_arguments():
    parser = argparse.ArgumentParser(description='Reportea a CSV desde CFDI (XML)')

    help = 'Archivo LOG. Por default en el directorio actual.'
    parser.add_argument('-log', '--archivo-log', help=help, default='')

    help = 'Directorio local donde se ubican los CFDI (XML)'
    parser.add_argument('-d', '--directorio',
        help=help, type=folder, required=True)

    path_home = util.get_home()
    default_file = util.join(path_home, 'reporte_cfdi.csv')
    help = 'Archivo CSV destino'
    parser.add_argument('-a', '--archivo',
        help=help, type=folder_write, default=default_file)

    help = 'Campos del reporte. Separados por pipe (|)'
    parser.add_argument('-c', '--campos', help=help, type=campos, required=True)

    help = 'CFDI de nomina.'
    parser.add_argument('-n', '--nomina', help=help, action='store_true')

    help = 'RFC del emisor.'
    parser.add_argument('-re', '--rfc-emisor', help=help, type=rfc, default='')

    help = 'RFC del receptor.'
    parser.add_argument('-rr', '--rfc-receptor', help=help, type=rfc, default='')

    help = 'Fecha inicial.\n\tFormatos aceptados:\n\tD/M/AAAA, ' \
        'D/M/AA, D-M-AAAA y D-M-AA'
    parser.add_argument('-fi', '--fecha-inicial', type=date, help=help)

    help = 'Fecha final. Mismos formatos de fecha inicial.'
    parser.add_argument('-ff', '--fecha-final', type=date, help=help)

    args=parser.parse_args()
    return args


def main():
    args = process_command_line_arguments()

    if args.archivo_log:
        formatter = logging.Formatter(LOG['FORMAT'], datefmt=LOG['DATE'])
        handler = RotatingFileHandler(
            args.archivo_log, maxBytes=2*1024*1024, backupCount=3, encoding='UTF-8')
        handler.setFormatter(formatter)
        if DEBUG:
            handler.setLevel(logging.DEBUG)
        else:
            handler.setLevel(logging.INFO)
        log.addHandler(handler)

    files = util.get_files(args.directorio)
    if files:
        print ('Total: {}'.format(len(files)))
        util.cfdi_reporte(files, args)
    else:
        msg = 'No se encontraron documentos para reportar en el directorio origen'
        print (msg)
    return


if __name__ == '__main__':
    main()

