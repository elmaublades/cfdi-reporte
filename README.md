## CFDI Reporte

Script para generar un reporte en CSV, directamente desde archivos XML, tanto CFDI como CFDI Nomina, además de verificar su estatus directamente en el SAT.

Si este software libre te es de utilidad, nos es muy valioso tu apoyo para
[nuestras actividades altruistas](http://universolibre.org/hacemos/) y para
seguir desarrollando herramientas como esta.

## Requerimientos

- Python 3.4+
- aiohttp

## Instalación

Usando git:

    git clone https://gitlab.com/mauriciobaeza/cfdi-reporte.git

O descargando la rama master: https://gitlab.com/mauriciobaeza/cfdi-reporte/repository/archive.zip?ref=master

Después de clonar o descargar y descomprimir:

    cd cfdi-reporte

## Uso

Consulta todos los parametros con

    ./cfdi-reporte -h

    usage: cfdi-reporte [-h] [-log ARCHIVO_LOG] -d DIRECTORIO [-a ARCHIVO] -c
                        CAMPOS [-n] [-re RFC_EMISOR] [-rr RFC_RECEPTOR]
                        [-fi FECHA_INICIAL] [-ff FECHA_FINAL]

    Reportea a CSV desde CFDI (XML)

    optional arguments:
      -h, --help            show this help message and exit
      -log ARCHIVO_LOG, --archivo-log ARCHIVO_LOG
                            Archivo LOG. Por default en el directorio actual.
      -d DIRECTORIO, --directorio DIRECTORIO
                            Directorio local donde se ubican los CFDI (XML)
      -a ARCHIVO, --archivo ARCHIVO
                            Archivo CSV destino
      -c CAMPOS, --campos CAMPOS
                            Campos del reporte. Separados por pipe (|)
      -n, --nomina          CFDI de nomina.
      -re RFC_EMISOR, --rfc-emisor RFC_EMISOR
                            RFC del emisor.
      -rr RFC_RECEPTOR, --rfc-receptor RFC_RECEPTOR
                            RFC del receptor.
      -fi FECHA_INICIAL, --fecha-inicial FECHA_INICIAL
                            Fecha inicial. Formatos aceptados: D/M/AAAA, D/M/AA,
                            D-M-AAAA y D-M-AA
      -ff FECHA_FINAL, --fecha-final FECHA_FINAL
                            Fecha final. Mismos formatos de fecha inicial.

Los argumentos mínimos son el directorio origen donde se encuentran los CFDI(XML), la busqueda es recursiva, y los campos del reporte separados por un pipe(|).

    ./cfdi-reporte -d /home/USER/cfdi -c 'uuid|fecha|emisor|receptor'

    10/06/2016 21:49:32 -CFDI-INFO - Iniciando reporte...
    10/06/2016 21:49:32 -CFDI-INFO - Obteniendo información de los XML...
    10/06/2016 21:49:32 -CFDI-INFO - Guardando archivo CSV: /home/USER/reporte_cfdi.csv
    10/06/2016 21:49:32 -CFDI-INFO - Reporte terminado...

Por default el archivo destino se llama `reporte_cfdi.csv` en la carpeta del usuario. Puedes establecer la ruta de este archivo.

    ./cfdi-reporte -d /home/USER/cfdi -a /home/USER/reportes/test.csv -c 'uuid|fecha|emisor|receptor'

Puedes establecer los siguientes filtros:

- -re RFC EMISOR
- -rr RFC RECEPTOR
- -fi FECHA INICIAL
- -ff FECHA FINAL

Puedes usar cualquier campo presente en los CFDI, los campos especiales son:

- emisor_rfc - Para obtener el RFC del emisor
- receptor_rfc - Para obtener el RFC del receptor
- conceptos - Te permite obtener todos los conceptos en una sola celda concatenados. Si quieres los valores separados, usa los campos de los conceptos de forma individual.
- impuesto_tipo_tasa - Para obtener un impuesto en especial, por ejemplo el IVA `traslado_iva_16`

Por default el sistema reporta solo CFDI, para reportar CFDI de Nómina, usa:

    ./cfdi-reporte -d /home/USER/cfdi -c 'uuid|fecha|emisor|receptor' -n

Claro, ahora puedes usar los campos de la nómina:

    ./cfdi-reporte -d /home/USER/cfdi -c 'uuid|fecha|emisor|receptor|imss|curp' -n

El script te reporteara todas las percepciones y deducciones automáticamente en forma
horizontal ordenadas por clave, así que NO incluyas ningún encabezado para estas.

Puedes verificar el estatus del documento directamente en el SAT, con solo agregar
el encabezado respectivo: `estatus_sat`

    ./cfdi-reporte -d /home/USER/cfdi -c 'uuid|fecha|emisor|receptor|estatus_sat'

Recuerda, si este software libre te es util, nos es muy valioso tu apoyo para
[nuestras actividades altruistas](http://universolibre.org/hacemos/) y para
seguir desarrollando herramientas como esta.
